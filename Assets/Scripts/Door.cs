﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Door : MonoBehaviour {

    public GameObject TextDisplay;
    public GameObject TheDoor;
    public float TheDistance = PlayerCast.DistanceFromTarget;
    public AudioSource click;

    void Update()
    {
        TheDistance = PlayerCast.DistanceFromTarget;
        if (Input.GetButtonDown("OpenDoor"))
        {
            click.Play();

            if (TheDistance <= 2)
            {
                StartCoroutine(OpenTheDoor());
            }
        }
    }

    void OnMouseOver()
    {
        if (TheDistance <= 2)
        {
            TextDisplay.GetComponent<Text>().text = "Press Button";
        }
    }

    void OnMouseExit()
    {
        TextDisplay.GetComponent<Text>().text = "";
    }

    IEnumerator OpenTheDoor()
    {
        TheDoor.GetComponent<Animator>().enabled = true;
        yield return new WaitForSeconds(1);
        TheDoor.GetComponent<Animator>().enabled = false;
        yield return new WaitForSeconds(5);
        TheDoor.GetComponent<Animator>().enabled = true;
        yield return new WaitForSeconds(1);
        TheDoor.GetComponent<Animator>().enabled = false;
    }

}