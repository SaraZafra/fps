﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour {

    public int EnemyHealth = 10;

    public GameObject Spider;

    public AudioSource Death;
    

    void DeductPoints(int DamageAmount)
    {
        EnemyHealth -= DamageAmount;
    }

    void Update()
    {
        if (EnemyHealth <= 0)
        {
            
            this.GetComponent<SpiderFollow>().enabled = false;
            Spider.GetComponent<Animation>().Play("death1");
            Death.Play();
            KillSpider();

        }
    }

    IEnumerator KillSpider()
    {
        yield return new WaitForSeconds(1f);
        Destroy(gameObject);
    }

   
}


