﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Health : MonoBehaviour {

    public static int PlayerHealth = 100;
    public int InternalHealth;
    public GameObject HealthDisplay;

    private void Update()
    {
        InternalHealth = PlayerHealth;
        HealthDisplay.GetComponent<Text>().text = "Health : " + PlayerHealth;
        if (PlayerHealth == 0)
        {
            SceneManager.LoadScene(3);
        }
    }

}
