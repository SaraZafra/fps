﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Ammo : MonoBehaviour {

    public static int LoadedAmmo;
    public int InternalLoaded;
    public GameObject LoadedDisplay;

    public static int CurrentAmmo;
    public int InternalAmmo;
    public GameObject AmmoDisplay;

    void Update()
    {
        InternalAmmo = CurrentAmmo;
        InternalLoaded = LoadedAmmo;
        AmmoDisplay.GetComponent<Text>().text = "" + InternalAmmo;
        LoadedDisplay.GetComponent<Text>().text = "" + LoadedAmmo;
    }
}