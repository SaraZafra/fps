﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameOver : MonoBehaviour {

    public AudioSource GameOverMusic;

    private void Start()
    {
        GameOverMusic.Play();
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
    }

   

    public void PlayAgain()
    {
        SceneManager.LoadScene("MainScene");
        Health.PlayerHealth = 100;
    }

    public void Exit()
    {
        Application.Quit();
    }

    public void MainMenu()
    {
        SceneManager.LoadScene(1);
    }

}
