﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;

public class Pause : MonoBehaviour {

    public bool Paused = false;
    public GameObject Player;
    public GameObject Gray;

    private void Update()
    {
        if (Input.GetButtonDown("Pause"))
        {
            if(Paused == false)
            {
                Gray.SetActive(true);
                Time.timeScale = 0;
                Paused = true;
                Player.GetComponent<FirstPersonController>().enabled = false;                
                Cursor.visible = true;
            }else
            {
                Gray.SetActive(false);
                Player.GetComponent<FirstPersonController>().enabled = true;
                Paused = false;
                Time.timeScale = 1;
            }
        }
    }
}
