﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AmmoPickUp : MonoBehaviour
{
    public AudioSource AmmoSound;
    

  


    void OnTriggerEnter(Collider other)
    {
        AmmoSound.Play();

        if (Ammo.LoadedAmmo == 0)
        {
            Ammo.LoadedAmmo += 10;
            this.gameObject.SetActive(false);
        }
        else
        {
            Ammo.CurrentAmmo += 10;
            this.gameObject.SetActive(false);
        }
    }
}

