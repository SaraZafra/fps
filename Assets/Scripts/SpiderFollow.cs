﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SpiderFollow : MonoBehaviour {

    public GameObject Player;
    public  GameObject Spider;
    public float TargetDistance;
    public float Range = 10;
    public float SpiderSpeed;
    public int Attack;
    public RaycastHit Shot;
    public Slider Healthbar;

    public static int isAttacking;
    public GameObject RedScreen;
    public AudioSource Hurt01;
    public AudioSource Hurt02;
    public AudioSource Hurt03;
    public AudioSource Attackmusic;
    public int HurtSound;


    private void Update()
    {
        transform.LookAt(Player.transform);
        if(Physics.Raycast (transform.position, transform.TransformDirection(Vector3.forward),out Shot))
        {
            TargetDistance = Shot.distance;
            if(TargetDistance < Range)
            {
                SpiderSpeed = 0.03f;
                if(Attack == 0)
                {
                    Spider.GetComponent<Animation>().Play("walk");
                    transform.position = Vector3.MoveTowards(transform.position, Player.transform.position,  Time.deltaTime);
                }
            }
            else
            {
                SpiderSpeed = 0;
                Spider.GetComponent<Animation>().Play("idle");
            }
        }
        if (Attack == 1)
        {
             
            Attackmusic.Play();
            if (isAttacking == 0)
            {
                StartCoroutine(EnemyDamage());
            }
            SpiderSpeed = 0;
            Spider.GetComponent<Animation>().Play("attack1");
            
        } 
   
    }
    void OnTriggerEnter(Collider other)
    {
        
        
        if (other.gameObject.tag == "Player")
        {                    
        Attack = 1;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        Attack = 0;
    }

    IEnumerator EnemyDamage()
    {
        isAttacking = 1;
        HurtSound = Random.Range(1, 4);
        yield return new WaitForSeconds(0.02f);
        RedScreen.SetActive(true);
        Health.PlayerHealth -= 10;
        Healthbar.value = Health.PlayerHealth;

        if (HurtSound == 1) {
            Hurt01.Play();
        }
        if (HurtSound == 2)
        {
            Hurt02.Play();
        }
        if (HurtSound == 3)
        {
            Hurt03.Play();
        }

        yield return new WaitForSeconds(0.05f);
        RedScreen.SetActive(false);
        yield return new WaitForSeconds(1);
        isAttacking = 0;
    }
}
