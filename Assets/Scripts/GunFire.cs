﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class GunFire : MonoBehaviour
{

    public GameObject Flash;

    // Update is called once per frame
    void Update()
    {
        if(Ammo.LoadedAmmo >= 1)
        {
       
        if (Input.GetButtonDown("Fire1") && Ammo.LoadedAmmo >= 1)
        {
            AudioSource gunsound = GetComponent<AudioSource>();
            gunsound.Play();
            Flash.SetActive(true);
            StartCoroutine(FlashOff());
            GetComponent<Animation>().Play("GunShot");
            Ammo.LoadedAmmo -= 1;
        }
        }
    }

    IEnumerator FlashOff()
    {
        yield return new WaitForSeconds(0.1f);
        Flash.SetActive(false);
    }
}
