﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Title : MonoBehaviour {



    IEnumerator ExecuteAfterTime (float time) { 

        yield return new WaitForSeconds(6);
        SceneManager.LoadScene(1);
    }

    public void Update()
    {
        StartCoroutine(ExecuteAfterTime(3));

        if (Input.GetButtonDown("Exit"))
        {
            Application.Quit();
        }
    }
}
