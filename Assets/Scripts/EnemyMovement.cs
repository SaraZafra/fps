﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMovement : MonoBehaviour {

    public GameObject Enemy;
    public GameObject Player;
    public float EnemySpeed;
    public int MoveTrigger;

    
    void Update()
    {
        if(MoveTrigger == 1) { 
        EnemySpeed = 0.09f;
        transform.position = Vector3.MoveTowards(transform.position, Player.transform.position, EnemySpeed);
    }
    }
}
