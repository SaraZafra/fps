﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour {

    public static Slider Healthbar;
    public int CurrentHealth;
    public GameObject Player;

    void ReduceHealth()
    {
        Health.PlayerHealth = Health.PlayerHealth - 5;
        Healthbar.value = Health.PlayerHealth;
        
    }
}
